package application;

import javafx.geometry.Pos;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.*;

public class MenuItem extends HBox{

	private static final Font FONT = Font.font("", FontWeight.BOLD, 18);
	private Text text;
	
	public MenuItem(String name) {
		super(15);
		setAlignment(Pos.CENTER);
		
		text = new Text(name);
		text.setFont(FONT);
		text.setEffect(new GaussianBlur(2));
		
		getChildren().addAll(text);
		setActive(false);
	}
	
	public void setActive(boolean b) {
		text.setFill(b ? Color.WHITE : Color.GREY);
	}
	
	public void changeColorRed() {
		text.setFill(Color.RED);
	}
	public void changeColorGrey() {
		text.setFill(Color.GREY);
	}
}
