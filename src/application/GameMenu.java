package application;

import enviroment.Controls;
import enviroment.GameMap;
import javafx.scene.layout.*;
import objects.Tank;

public class GameMenu extends Pane {
	
	
	public GameMenu(Tank tank1, Tank tank2, GameMap map, Controls control) throws Exception{
		
		
		this.setPrefSize(1200, 800);
		this.getChildren().addAll(map, control);
		this.getChildren().add(tank1);
		this.getChildren().add(tank2);
		this.getChildren().add(tank1.getImg());
		this.getChildren().add(tank2.getImg());
		
	}

	
}
