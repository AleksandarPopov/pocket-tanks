package weapons;

public enum WeaponType {

	SINGLE_SHOT, CUTTER;
	
	public static String getName(WeaponType type){
		
		if(type.equals(SINGLE_SHOT)) {
			return "Single Shot";
		}
		else if(type.equals(CUTTER)) {
			return "Cutter";
		}
		else {
			System.err.println("Weapon not registered in WeaponType.");
			return "Not-Registered-Weapon";
		}
		
	}
	
}
