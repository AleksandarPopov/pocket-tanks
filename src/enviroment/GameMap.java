package enviroment;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;


public class GameMap extends BorderPane{

	public Text connection = new Text("konekcija");
	private int player1Score = 0;
	private int player2Score = 0;
	private Text score1 = new Text(("Player 1:\n " + player1Score));
	private Text score2 = new Text(("Player 2:\n " + player2Score));
	private double x = 0;
	private double y = 550;
	private Rectangle recPlayer1;
	private Rectangle recPlayer2;
	private Rectangle recCenter;
	private Rectangle recBottom;
	private Rectangle recKonekcija = new Rectangle(0, 0, 1200, 18);
	private final Image im1 = new Image("file:images/bricks/brick1.png");
	
	public GameMap() {
		
		recKonekcija = new Rectangle(0, 0, 1200, 18);
		recKonekcija.setFill(Color.RED);
		recCenter = new Rectangle(0, 18, 1200, 532);
		recCenter.setFill(Color.AQUAMARINE);
		recPlayer1 = new Rectangle(0, 18, 50, 35);
		recPlayer1.setFill(Color.GREEN);
		recPlayer2 = new Rectangle(1150, 18, 50, 35);
		recPlayer2.setFill(Color.GREEN);
		recBottom = new Rectangle(0, 550, 1200, 50);
		recBottom.setFill(Color.BLUE);
		
		this.getChildren().addAll(recKonekcija, recCenter, recPlayer1, recPlayer2, recBottom);
		for(int i = 0; i<12; i++) {
			ImageView image2 = new ImageView();
			image2.setImage(im1);
			image2.setFitHeight(50);
			image2.setFitWidth(100);
			image2.setLayoutX(x);
			image2.setLayoutY(y);
			this.getChildren().add(image2);
			x += 100;
		}
		this.setPrefSize(1200, 600);
		this.setLayoutX(0);
		this.setLayoutY(0);
		this.setTop(connection);
		this.setAlignment(connection, Pos.TOP_CENTER);
		this.setLeft(score1);
		this.setAlignment(score1, Pos.TOP_CENTER);
		this.setRight(score2);
		this.setAlignment(score2, Pos.TOP_CENTER);
		
		
	}
	
	public int getPlayer1Score() {
		return player1Score;
	}

	public void setPlayer1Score(int player1Score) {
		this.player1Score = player1Score;
	}

	public int getPlayer2Score() {
		return player2Score;
	}

	public void setPlayer2Score(int player2Score) {
		this.player2Score = player2Score;
	}

	public Text getScore1() {
		return score1;
	}

	public Text getScore2() {
		return score2;
	}

	public Rectangle getRecKonekcija() {
		return recKonekcija;
	}
	public Rectangle getRecCenter() {
		return recCenter;
	}
	
}
