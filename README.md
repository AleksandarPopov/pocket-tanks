# Pocket Tanks

Multiplayer game made using JavaFX. The point of this game is witch player will score more points with limited ammo and different types of ammo. Game features turn based play. Server is GUI based showing player actions.

# Project Title

Pocket Tanks game is made using famous game worms.

## Getting Started

This was my school game project for last exam. There are some minor bugs to fix but game is working.

### Prerequisites

What things you need to install the software and how to install them.  
You need to have some IDE for java installed on your system(**Eclipse**, **NetBeans**, ...).  
Also you need to download **JDK 1.8.0_221** and add it to the project.    

### Installing

Since there is no installation in order to use Pocket Tanks you need to download it from GitLab from [here](https://gitlab.com/CriticalNoob/pocket-tanks).  

## Deployment
To be able to play Pocket Tanks you need to follow next steps:  

**Step 1:** Download project to your system  
**Step 2:** Add project to your IDE  
**Step 3:** Add downloaded JDK file like this:  
```  
Right click on project name > Build path > add External JARs..  
```  
**Step 4:** Run Server from server folder  
**Step 5:** Run client from application folder class name Main  
**Step 6:** Run another client like in step 5 and play the game  


## Authors

* **Aleksandar Popov** - *Initial work* - [CriticalNoob](https://gitlab.com/CriticalNoob)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

